"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _rangeCss = _interopRequireDefault(require("./range.css.js"));

var _reactDom = _interopRequireDefault(require("react-dom"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Slider = /*#__PURE__*/function (_Component) {
  _inherits(Slider, _Component);

  var _super = _createSuper(Slider);

  function Slider(props) {
    var _this;

    _classCallCheck(this, Slider);

    _this = _super.call(this, props);
    var value = _this.props.value ? _this.props.value : props.multiple ? _toConsumableArray(props.settings.start) : props.settings.start;
    var offset = 10;
    _this.state = {
      value: value,
      position: props.multiple ? [] : 0,
      momentsPositions: props.settings.moments ? props.settings.moments.map(function (m) {
        return offset;
      }) : [],
      numberOfThumbs: props.multiple ? value.length : 1,
      offset: offset,
      precision: 0,
      mouseDown: false
    };
    _this.determinePosition = _this.determinePosition.bind(_assertThisInitialized(_this));
    _this.rangeMouseUp = _this.rangeMouseUp.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Slider, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.determinePrecision();
      var value = this.props.value ? this.props.value : this.state.value;
      this.setValuesAndPositions(value, false);
      window.addEventListener("mouseup", this.rangeMouseUp);
      this.interval = setInterval(function () {
        if (!!_this2.interval && !!_this2.inner && !!_this2.inner.offsetWidth) {
          _this2.setValuesAndPositions(_this2.state.value, true);

          clearInterval(_this2.interval);
        }
      }, 100);
    }
  }, {
    key: "UNSAFE_componentWillReceiveProps",
    value: function UNSAFE_componentWillReceiveProps(nextProps) {
      if (nextProps.value >= 0 && nextProps.value !== this.state.value) {
        if (this.props.multiple) {
          var different = this.isDifferentArrays(nextProps.value, this.state.value);

          if (different) {
            this.setValuesAndPositions(nextProps.value, true);
          }
        } else {
          this.setValuesAndPositions(nextProps.value, true);
        }
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.inner = undefined;
      this.innerLeft = undefined;
      this.innerRight = undefined;
      window.removeEventListener("mouseup", this.rangeMouseUp);
    }
  }, {
    key: "setValuesAndPositions",
    value: function setValuesAndPositions(value, triggeredByUser) {
      var _this3 = this;

      if (this.props.multiple) {
        var positions = _toConsumableArray(this.state.position);

        value.forEach(function (val, i) {
          _this3.setValue(val, triggeredByUser, i);

          positions[i] = _this3.determinePosition(val);
        });
        this.setState({
          position: positions
        });
      } else {
        this.setValue(value, triggeredByUser);
        this.setState({
          position: this.determinePosition(value)
        });
      } // Set moments positions


      if (this.props.settings.moments) {
        var momentsPositions = [];
        this.props.settings.moments.forEach(function (moment) {
          var momentOffset;

          switch (moment.event) {
            case 'submit':
              momentOffset = 0;
              break;

            case 'run':
              momentOffset = 15;
              break;

            default:
              momentOffset = 2;
          }

          momentsPositions.push(_this3.determinePosition(moment.time) + _this3.state.offset - momentOffset);
        });
        this.setState({
          momentsPositions: momentsPositions
        });
      }
    }
  }, {
    key: "isDifferentArrays",
    value: function isDifferentArrays(a, b) {
      var different = false;
      a.some(function (val, i) {
        if (val !== b[i]) {
          different = true;
          return true;
        }
      });
      return different;
    }
  }, {
    key: "determinePosition",
    value: function determinePosition(value) {
      var trackLeft = _reactDom["default"].findDOMNode(this.track).getBoundingClientRect().left;

      var innerLeft = _reactDom["default"].findDOMNode(this.inner).getBoundingClientRect().left;

      var ratio = (value - this.props.settings.min) / (this.props.settings.max - this.props.settings.min);
      var position = Math.round(ratio * this.inner.offsetWidth) + trackLeft - innerLeft - this.state.offset;
      return position;
    }
  }, {
    key: "determinePrecision",
    value: function determinePrecision() {
      var split = String(this.props.settings.step).split(".");
      var decimalPlaces;

      if (split.length === 2) {
        decimalPlaces = split[1].length;
      } else {
        decimalPlaces = 0;
      }

      this.setState({
        precision: Math.pow(10, decimalPlaces)
      });
    }
  }, {
    key: "determineValue",
    value: function determineValue(startPos, endPos, currentPos) {
      var ratio = (currentPos - startPos) / (endPos - startPos);
      var range = this.props.settings.max - this.props.settings.min;
      var difference = Math.round(ratio * range / this.props.settings.step) * this.props.settings.step; // Use precision to avoid ugly Javascript floating point rounding issues
      // (like 35 * .01 = 0.35000000000000003)

      difference = Math.round(difference * this.state.precision) / this.state.precision;
      return difference + this.props.settings.min;
    }
  }, {
    key: "determineThumb",
    value: function determineThumb(position, value) {
      if (!this.props.multiple) {
        return 0;
      }

      if (position <= this.state.position[0]) {
        return 0;
      }

      if (position >= this.state.position[this.state.numberOfThumbs - 1]) {
        return this.state.numberOfThumbs - 1;
      }

      var index = 0;

      for (var i = 0; i < this.state.numberOfThumbs - 1; i++) {
        if (position >= this.state.position[i] && position < this.state.position[i + 1]) {
          var distanceToSecond = Math.abs(position - this.state.position[i + 1]);
          var distanceToFirst = Math.abs(position - this.state.position[i]);

          if (distanceToSecond <= distanceToFirst) {
            return i + 1;
          } else {
            return i;
          }
        }
      }

      return index;
    }
  }, {
    key: "setValue",
    value: function setValue(value, triggeredByUser, thumbIndex) {
      if (typeof triggeredByUser === "undefined") {
        triggeredByUser = true;
      }

      var currentValue = this.props.multiple ? this.state.value[thumbIndex] : this.state.value;

      if (currentValue !== value) {
        var newValue = [];

        if (this.props.multiple) {
          newValue = _toConsumableArray(this.state.value);
          newValue[thumbIndex] = value;
          this.setState({
            value: newValue
          });
        } else {
          newValue = value;
          this.setState({
            value: value
          });
        }

        if (this.props.settings.onChange) {
          this.props.settings.onChange(newValue, {
            triggeredByUser: triggeredByUser
          });
        }
      }
    }
  }, {
    key: "setValuePosition",
    value: function setValuePosition(value, triggeredByUser, thumbIndex) {
      if (this.props.multiple) {
        var positions = _toConsumableArray(this.state.position);

        positions[thumbIndex] = this.determinePosition(value);
        this.setValue(value, triggeredByUser, thumbIndex);
        this.setState({
          position: positions
        });
      } else {
        this.setValue(value, triggeredByUser);
        this.setState({
          position: this.determinePosition(value)
        });
      }
    }
  }, {
    key: "setPosition",
    value: function setPosition(position, thumbIndex) {
      if (this.props.multiple) {
        var newPosition = _toConsumableArray(this.state.position);

        newPosition[thumbIndex] = position;
        this.setState({
          position: newPosition
        });
      } else {
        this.setState({
          position: position
        });
      }
    }
  }, {
    key: "rangeMouseDown",
    value: function rangeMouseDown(isTouch, e) {
      e.stopPropagation();

      if (!this.props.disabled) {
        if (!isTouch) {
          e.preventDefault();
        }

        this.setState({
          mouseDown: true
        });

        var innerBoundingClientRect = _reactDom["default"].findDOMNode(this.inner).getBoundingClientRect();

        this.innerLeft = innerBoundingClientRect.left;
        this.innerRight = this.innerLeft + this.inner.offsetWidth;
        this.rangeMouse(isTouch, e);
      }
    }
  }, {
    key: "rangeMouse",
    value: function rangeMouse(isTouch, e) {
      var pageX;
      var event = isTouch ? e.touches[0] : e;

      if (event.pageX) {
        pageX = event.pageX;
      } else {
        console.log("PageX undefined");
      }

      var value = this.determineValue(this.innerLeft, this.innerRight, pageX);

      if (pageX >= this.innerLeft && pageX <= this.innerRight) {
        if (value >= this.props.settings.min && value <= this.props.settings.max) {
          var position = pageX - this.innerLeft - this.state.offset;
          var thumbIndex = this.props.multiple ? this.determineThumb(position) : undefined;

          if (this.props.discrete) {
            this.setValuePosition(value, false, thumbIndex);
          } else {
            this.setPosition(position, thumbIndex);
            this.setValue(value, undefined, thumbIndex);
          }
        }
      }
    }
  }, {
    key: "rangeMouseMove",
    value: function rangeMouseMove(isTouch, e) {
      e.stopPropagation();

      if (!isTouch) {
        e.preventDefault();
      }

      if (this.state.mouseDown) {
        this.rangeMouse(isTouch, e);
      }
    }
  }, {
    key: "rangeMouseUp",
    value: function rangeMouseUp() {
      this.setState({
        mouseDown: false
      });
    }
  }, {
    key: "renderMoments",
    value: function renderMoments() {
      var _this4 = this;

      return this.props.settings.moments.map(function (moment, index) {
        var styleKey;

        switch (moment.event) {
          case "submit":
            styleKey = "submitMoment";
            break;

          case "run":
            styleKey = "runMoment";
            break;

          default:
            styleKey = "moment";
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          key: "moment-".concat(index),
          style: _objectSpread(_objectSpread(_objectSpread({}, _rangeCss["default"][styleKey]), _this4.props.style ? _this4.props.style[styleKey] ? _this4.props.style[styleKey] : {} : {}), {
            left: _this4.state.momentsPositions[index] + "px"
          })
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", {
        onMouseDown: function onMouseDown(event) {
          return _this5.rangeMouseDown(false, event);
        },
        onMouseMove: function onMouseMove(event) {
          return _this5.rangeMouseMove(false, event);
        },
        onMouseUp: function onMouseUp(event) {
          _this5.rangeMouseUp(false, event);

          if (_this5.props.settings.onSlide) {
            _this5.props.settings.onSlide(_this5.state.value);
          }
        },
        onTouchEnd: function onTouchEnd(event) {
          _this5.rangeMouseUp(true, event);

          if (_this5.props.settings.onSlide) {
            _this5.props.settings.onSlide(_this5.state.value);
          }
        },
        onTouchMove: function onTouchMove(event) {
          return _this5.rangeMouseMove(true, event);
        },
        onTouchStart: function onTouchStart(event) {
          return _this5.rangeMouseDown(true, event);
        },
        style: _objectSpread(_objectSpread(_objectSpread({}, _rangeCss["default"].range), this.props.disabled ? _rangeCss["default"].disabled : {}), this.props.style ? this.props.style : {})
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "semantic_ui_range_inner",
        ref: function ref(inner) {
          _this5.inner = inner;
        },
        style: _objectSpread(_objectSpread({}, _rangeCss["default"].inner), this.props.style ? this.props.style.inner ? this.props.style.inner : {} : {})
      }, /*#__PURE__*/_react["default"].createElement("div", {
        ref: function ref(track) {
          _this5.track = track;
        },
        style: _objectSpread(_objectSpread(_objectSpread({}, _rangeCss["default"].track), this.props.inverted ? _rangeCss["default"].invertedTrack : {}), this.props.style ? this.props.style.track ? this.props.style.track : {} : {})
      }), /*#__PURE__*/_react["default"].createElement("div", {
        ref: function ref(trackFill) {
          _this5.trackFill = trackFill;
        },
        style: _objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({}, _rangeCss["default"].trackFill), this.props.inverted ? _rangeCss["default"].invertedTrackFill : {}), _rangeCss["default"][this.props.inverted ? "inverted-" + this.props.color : this.props.color]), this.props.style ? this.props.style.trackFill ? this.props.style.trackFill : {} : {}), this.props.disabled ? _rangeCss["default"].disabledTrackFill : {}), this.props.style ? this.props.style.disabledTrackFill ? this.props.style.disabledTrackFill : {} : {}), {
          width: this.state.position + this.state.offset + "px"
        }), this.props.multiple && this.state.position.length > 0 ? {
          left: this.state.position[0],
          width: this.state.position[this.state.numberOfThumbs - 1] - this.state.position[0]
        } : {})
      }), this.props.multiple ? this.state.position.map(function (pos, i) {
        return /*#__PURE__*/_react["default"].createElement("div", {
          key: i,
          style: _objectSpread(_objectSpread(_objectSpread({}, _rangeCss["default"].thumb), _this5.props.style ? _this5.props.style.thumb ? _this5.props.style.thumb : {} : {}), {
            left: pos + "px"
          })
        });
      }) : /*#__PURE__*/_react["default"].createElement("div", {
        style: _objectSpread(_objectSpread(_objectSpread({}, _rangeCss["default"].thumb), this.props.style ? this.props.style.thumb ? this.props.style.thumb : {} : {}), {
          left: this.state.position + "px"
        })
      }), this.renderMoments().map(function (moment) {
        return moment;
      }))));
    }
  }]);

  return Slider;
}(_react.Component);

exports["default"] = Slider;
Slider.defaultProps = {
  color: "red",
  settings: {
    min: 0,
    max: 10,
    step: 1,
    start: 0
  }
};
Slider.propTypes = {
  color: _propTypes["default"].string,
  disabled: _propTypes["default"].bool,
  discrete: _propTypes["default"].bool,
  inverted: _propTypes["default"].bool,
  multiple: _propTypes["default"].bool,
  settings: _propTypes["default"].shape({
    min: _propTypes["default"].number,
    max: _propTypes["default"].number,
    step: _propTypes["default"].number,
    start: _propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].arrayOf(_propTypes["default"].number)]),
    onChange: _propTypes["default"].func,
    onSlide: _propTypes["default"].func,
    moments: _propTypes["default"].arrayOf(_propTypes["default"].shape({
      time: _propTypes["default"].number,
      event: _propTypes["default"].string
    }))
  })
};